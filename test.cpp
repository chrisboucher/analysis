#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sys/time.h>
#include <time.h>
using namespace std;


int main(int argc, char ** argv) {
   if (argc != 3) return 1;
  size_t n = (size_t) strtoul(argv[1], NULL, 10);
  string outfile = string(argv[2]);
  fstream f(outfile.c_str(), ios::out);
  struct timeval tv; 
  gettimeofday(&tv, NULL);
  time_t t = tv.tv_sec;
  cerr << ctime(&t); 
  cerr << "TEST ERROR STREAM" << endl;
  if (f.good()) {
    for (size_t i = 0; i < n; i++) f << (float) random() / (float) RAND_MAX << endl;
    f.close();
  } else {
    cerr << "Unable to open file test.csv" << endl;
  }
  
  return 0;
}
