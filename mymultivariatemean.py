import model_api
from sensetdp.parsers import PandasObservationParser
from sensetdp.models import Observation, UnivariateResult
import json
import pandas as pd
import os
import subprocess 
import numpy as np

app = model_api.Application()

def exectest(path, n):
    #test external exe write / read
    testcsvfn = path + "test.csv"
    cmd = path + "test " + str(n) + " " + testcsvfn
    status = subprocess.call(cmd, shell=True)
    csvrand = pd.read_csv(testcsvfn)


@app.model('mymultivariatemean')
def mymultivariatemean(context):
    input_stream_ids = context.ports['inputs'].stream_ids
    output_stream_id = context.ports['output'].stream_id
    
    params_doc = getattr(context.ports.get("params"), 'document', None) or '{}'
    params = json.loads(params_doc)

    # Obtain observation data from `inputs` streams.
    context.update(message='Loading data...')
    limit = 10000
    start = params["start"]
    end = params["end"]
    samplerule = params['samplerule']
    while True:
        data = context.sc_api.get_observations(streamid=','.join(input_stream_ids), media='csv', limit=limit, start=start, end=end, parser=PandasObservationParser())
        if len(data) < limit:
            break
        limit *= 10

    context.update(message='Producing resampled output ...')         
    # Compute resampled mean and store in `output` stream..
    ids = []
    means = pd.DataFrame()
    for stream_id in input_stream_ids:
        ids.append(stream_id)
        means[stream_id] = data[stream_id].resample(rule=samplerule).mean()
        means[stream_id] = means[stream_id].fillna(method='pad')
        #means[stream_id] = means[stream_id].apply(np.round)
        print stream_id + " " + str(means[stream_id].size)
        
    # set path - context not setting env properly 20161025
    if not hasattr(context, "mypath"):
        context.mypath = "/opt/model/"
    cmd = "mkdir -p " + context.mypath + "data"
    status = subprocess.call(cmd, shell=True)
    

    context.update(message='Writing resampled data to application schedule file ...')
    schedfile = "data/schedule.csv"
    meanscsvfn = context.mypath + schedfile
    means.to_csv(meanscsvfn, header=False)
    csvmeans = pd.read_csv(meanscsvfn, header=0, names=["dt", "dbt", "rh"])
    
    #exectest(context.mypath, means[ids[0]].size)

    context.update(message='Setting up and running yarddrying application ...')         
    cmd = context.mypath + "yarddrying -h 240 -K -R -S " + context.mypath + schedfile + " > " + context.mypath + "debug.txt"
    print cmd
    #status = subprocess.call(cmd, shell=True)

    mean = means[ids[0]]
    output = Observation()
    output.results = [UnivariateResult(t=t.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),v=v) for t,v in zip(mean.index, mean)]
    context.sc_api.create_observations(output, streamid=output_stream_id)

if not app.run():
    from sensetdp.api import API
    from sensetdp.auth import HTTPBasicAuth
    from as_client import Client
    import sys
    
    class Context(object):
        pass
            
    class Port(object):
        def __init__(self, type_, direction):
            self.type = type_
            self.direction = direction
                
    class StreamPort(Port):
        def __init__(self, direction, stream_id):
            self.stream_id = stream_id
            super(StreamPort, self).__init__('stream', direction)
                
    class MultistreamPort(Port):
        def __init__(self, direction, stream_ids):
            self.stream_ids = stream_ids
            super(MultistreamPort, self).__init__('multistream', direction)
            
    class DocumentPort(Port):
        def __init__(self, direction, document):
            self.document = document
            super(DocumentPort, self).__init__('document', direction)
                
    context = Context()
    context.model_id = 'mymultivariatemean'
    context.ports = {
        'inputs': MultistreamPort('input', ['forest_and_wood.ForestProducts.Test2.AT_DS2', 
                                            'forest_and_wood.ForestProducts.Test2.RH',
                                            'forest_and_wood.ForestProducts.Test2.WS_AVE']),
        'params': DocumentPort('input', '{"start": "2016-10-10T00:00:00.000Z", "end" : "2016-12-31T00:00:00.000Z", "samplerule" : "D"}'),
        'output': StreamPort('output', 'forest_and_wood.AWS1.air_temperature-DS2_AT.daily')
    }

    context.sc_api = API(HTTPBasicAuth(sys.argv[1], sys.argv[2]), 'staging.data.sense-t.org.au')
    context.as_api = Client('https://staging.data.sense-t.org.au/api/analysis/v2', (sys.argv[1], sys.argv[2]))
    context.update = lambda progress=None, message=None: None
    context.terminate = lambda: None
    context.mypath = "./"
    mymultivariatemean(context)

